import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Form from './form/Form';

export default class Index extends Component{
    render(){
            return (
            <div className="getting-started">
                <h4 className="title mb-4">Wunderfleet</h4>
                <div className="description mb-3">You need to provide some information in order to complete the registration process</div>
                <button id="btnRegister" type="button"className="btn btn-md btn-primary mb-1" data-toggle="modal" data-target="#formModal">Get started</button>
                <div className="registration-status"><span>N</span> question(s) left</div>
                <div className="registration-status"><i className="fas fa-check success"></i> Registration completed</div>

                <div className="modal fade" id="formModal" tabIndex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                               <Form />
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        );
    }
}

if(document.getElementById('app')){
    ReactDOM.render(<Index />, document.getElementById('app'));
}

