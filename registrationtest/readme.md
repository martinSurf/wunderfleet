## Technology used: 

Laravel 5 framework backend
ReactJS library frontEnd
DataBase sqlite

Please check the db here > https://drive.google.com/file/d/1kT1L-dCRnI-GtdftSlCj01P7-k079gTk/view?usp=sharing


## Why did I choose Laravel?

I have little time working with Laravel, but that little time showed me the easy that it is to understand the sintax and how it works. So that for me is a big advantage because if somebody adds up to the project it won't take long time for this person to start programming with you even if he doesn't know laravel (Just needs to know PHP).

Another advantage is that Laravel uses powerful object-oriented design features to help you architect a well-structured backend. Talking about the structure of Laravel supports MVC architecture. For me is great to work with MVC pattern because it helps you tier your code for easier maintenance, also ensures clarity between logic and presentation.

## Why did I use react js ?

React JS library helps to create SPAs, but not only SPAs also MPAs and that is an advantage in case the app needs to have more multiple views.

It facilitates the overall process of writing components, this is the main reason why did I choose working with react, this is a really cool feature it boosts productivity and facilitates further maintenance.


## Code optimizations

I put all the functions in one JS file, I don't like that because it breaks the S (single responsability) in the Solid principle, also I would like to separate the functions inside the app.js file because a lot of things are done in one single funtion. 

In php I did not handle exceptions, I can catch exceptions in the core functions like save and update in order to make sure everything is working as suppoused.

## What I could have done better?

when I started with the project I thought okay I will use a simple database with one table to save the payment but the ideal way for me is to create a table with the detail of the payment by client. 
Meaning to have 2 tables Client and Payment.

Please take a look at the image here: https://drive.google.com/file/d/1bgZcmqI8YVtNJTaMQWYALNde0qTZJMnE/view?usp=sharing

I think I can optimize the front-end make it look better, of course optimize my js and jsquery code and files.  

Also I would have loved to use dockers in order to have a container for Composer and another for nodeJS, this would help to the development and maintainance of the project. 