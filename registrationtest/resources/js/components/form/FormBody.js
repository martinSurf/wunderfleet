import React, { Component } from 'react';

export default class FormBody extends Component{
    render(){
        return(
            <div className="col-lg-8">
                <div className="tab-content text-left">
                    <div className="tab-pane active" id="personalInfo">
                        <div className="panel panel-default">
                                <div className="panel-body">
                                    <div className="form-group">
                                        <label>First Name</label>
                                        <input type="text" name="firstName" id="firstName" className="form-control" placeholder="First Name"/>
                                        <span id="error_name" className="text-danger"></span>
                                    </div>
                                    <div className="form-group">
                                        <label>Last Name</label>
                                        <input type="text" name="lastName" id="lastName" className="form-control" placeholder="Last Name"/>
                                        <span id="error_lname" className="text-danger"></span>
                                    </div>
                                    <div className="form-group">
                                        <label>Telephone</label>
                                        <input type="text" name="telephone" id="telephone" className="form-control" placeholder="Telephone Number"/>
                                        <span id="error_telephone" className="text-danger"></span>
                                    </div>
                                    <br />
                                    <div className="row">
                                        <div className="col-md-6">
                                            <button type="button" name="btnPersonalInfo" id="btnPersonalInfo" className="btn btn-default btn-md">Next <i className="fas fa-angle-right"></i></button>
                                        </div>
                                        <div className="col-md-6 text-right">
                                            <button type="button" className="btn btn-danger btn-md" data-dismiss="modal" id="btnPersonalInfoCancel">Cancel Registration</button>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div className="tab-pane fade" id="addressInfo">
                    <div className="panel panel-default">
                                <div className="panel-body">
                                    <div className="form-group">
                                        <label>Street Name</label>
                                        <input type="text" name="streetName" id="streetName" className="form-control" placeholder="Street Name"/>
                                        <span id="error_stName" className="text-danger"></span>
                                    </div>
                                    <div className="form-group">
                                        <label>Street Number</label>
                                        <input type="text" name="streetNumber" id="streetNumber" className="form-control" placeholder="Street Number"/>
                                        <span id="error_stNumber" className="text-danger"></span>
                                    </div>
                                    <div className="form-group">
                                        <label>Zip code</label>
                                        <input type="text" name="zip" id="zip" className="form-control" placeholder="Zip code"/>
                                        <span id="error_zip" className="text-danger"></span>
                                    </div>
                                    <div className="form-group">
                                        <label>City</label>
                                        <input type="text" name="city" id="city" className="form-control" placeholder="City name"/>
                                        <span id="error_city" className="text-danger"></span>
                                    </div>
                                    <br />
                                    <div className="row">
                                        <div className="col-md-6">
                                            <button type="button" name="previousAddressInfo" id="previousAddressInfo" className="btn btn-default btn-md mr-2"><i className="fas fa-angle-left"></i> Previous</button>
                                            <button type="button" name="btnAddressInfo" id="btnAddressInfo" className="btn btn-default btn-md">Next <i className="fas fa-angle-right"></i></button>
                                        </div>
                                        <div className="col-md-6 text-right">
                                            <button type="button" className="btn btn-danger btn-md" data-dismiss="modal" id="btnAddressInfoCancel">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div className="tab-pane fade" id="paymentInfo">
                    <div className="panel panel-default">
                                <div className="panel-body">
                                    <div className="form-group">
                                        <label>Account owner</label>
                                        <input type="text" name="accountOwner" id="accountOwner" className="form-control" placeholder="Account owner"/>
                                        <span id="error_accOwner" className="text-danger"></span>
                                    </div>
                                    <div className="form-group">
                                        <label>International Account Number (IBAN)</label>
                                        <input type="text" name="iban" id="iban" className="form-control" placeholder="GB15HBUK4012761235678"/>
                                        <span id="error_IBAN" className="text-danger"></span>
                                    </div>            
                                    <br />
                                    <div className="row">
                                        <div className="col-md-6">
                                            <button type="button" name="previousPaymentInfo" id="previousPaymentInfo" className="btn btn-default btn-md"><i className="fas fa-angle-left"></i> Previous</button>
                                        </div>
                                        <div className="col-md-6 text-right">
                                            <button type="button" name="btnPaymentInfo" id="btnPaymentInfo" className="btn btn-success btn-md mr-2">Register</button>
                                            <button type="button" className="btn btn-danger btn-md" data-dismiss="modal" id="btnPaymentInfoCancel">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div className="tab-pane fade" id="result">
                        <div className="panel panel-default">
                                <div className="panel-body">
                                        <div id="resultAlert">
                                            <h4 id="resultAlertHeading"></h4>
                                            <p id="resultAlertMsg" style={{wordBreak: "break-all"}}></p>
                                            <br />
                                        </div>
                                        <br />
                                        <div align="center">
                                            <button type="button" name="btnResultInfo" id="btnResult" className="btn btn-success btn-lg" data-dismiss="modal">Close</button>
                                        </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
