<?php

namespace App\Http\Controllers;

use App\Registration;
use Illuminate\Http\Request;
use \DateTime;
use GuzzleHttp\Client;


class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('registration');
    }

    public function register(Request $request){
        if($request->ajax()) {

            $link = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';

            //Database save Costumer Info
             $customer = $this->saveRegistration($request);
             
            //Call API FROM WunDERFLEET
            $WunderApi = new Client(['headers' => ['content-type'=> 'application/json', 'Accept' => 'application/json']]);
    
            $apiResponse = $WunderApi->request('POST',$link,[
                'json' => [
                        'customerId' =>$customer['id'],
                        'iban'=>$customer['iban'],
                        'owner'=>$customer['accountOwner']
                    ],
            ]);
                
            //Here I get the apiResponse from de Wunder API
            $apiDataBody = $apiResponse->getBody();
            $apiDataStatus = $apiResponse->getStatusCode();
            $apiDataBody = json_decode($apiDataBody);
            $paymentDataID = $apiDataBody->paymentDataId;

            if($apiDataStatus == 200){
                //Save paymentDataId in the customer registration table
                $this->savePaymentDataId($customer['id'],$paymentDataID);
                $msg = "You have registered you account successfully this is your account data Id: " . $paymentDataID;
                $response = response()->json(['msg'=> $msg, 'status'=>$apiDataStatus]);
            }else {
                $msg = "There was a problem with the API";
                $response =  response()->json(['msg'=> $msg, 'status'=>$apiDataStatus]);
            }
        }
        return $response;
    }

    function saveRegistration($request){
            $registration = new Registration();
            $address = $request->streetName . " " . $request->streetNumber;
            $registration->name = $request->name;
            $registration->lastName = $request->lastName;
            $registration->telephone = $request->telephone;
            $registration->address = $address;
            $registration->zip = $request->zip;
            $registration->city = $request->city;
            $registration->accountOwner = $request->accountOwner;
            $registration->iban = $request->iban;
            $registration->created_at = new DateTime(getdate()['year']."-".getdate()['mon']."-".getdate()['mday']);
            $registration->save();

            $id = $registration->id;
            $iban = $registration->iban;
            $accountOwner = $registration->accountOwner;
                    
            $customer = [
                'id' => $id,
                'iban' => $iban,
                'accountOwner' => $accountOwner
            ];

        return $customer;
    }

    function savePaymentDataId($customerId,$paymentDataID){
        $registration = new Registration(); 
        $updateRegister = $registration::find($customerId);
        $updateRegister->paymentDataId = $paymentDataID;
        $updateRegister->save();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Registration  $registration
     * @return \Illuminate\Http\Response
     */
    public function show(Registration $registration)
    {
        //
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Registration  $registration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Registration $registration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Registration  $registration
     * @return \Illuminate\Http\Response
     */
    public function destroy(Registration $registration)
    {
        //
    }
}
