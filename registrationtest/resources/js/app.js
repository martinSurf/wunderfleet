
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('./components/Index');

$(document).ready(function(){
    //Handling cookies
    var firstName = Cookies.get('firstName');
    var lastName = Cookies.get('lastName');
    var telephone = Cookies.get('telephone');
    var streetName = Cookies.get('streetName');
    var streetNumber = Cookies.get('streetNumber');
    var zip = Cookies.get('zip');
    var city = Cookies.get('city');
    var accountOwner = Cookies.get('accountOwner');
    var iban = Cookies.get('iban');
    var tab = Cookies.get('tab');

    if(tab != ""){
        switch(tab){
            case '1': $('#firstName').val(firstName);
                    $('#lastName').val(lastName);
                    $('#telephone').val(telephone);
            
                    $('#personal_tab').removeClass('active');
                    $('#personal_tab').removeAttr('href data-toggle');
                    $('#personalInfo').removeClass('active');
                    $('#personalInfo').addClass('fade');
                    
                    $('#address_tab').removeClass('disabled');
                    $('#address_tab').addClass('active');
                    $('#address_tab').attr('href', '#addressInfo');
                    $('#address_tab').attr('data-toggle', 'tab');
                    $('#addressInfo').removeClass('fade');
                    $('#addressInfo').addClass('active');
                    break;
            
            case '2':$('#firstName').val(firstName);
                    $('#lastName').val(lastName);
                    $('#telephone').val(telephone); 
                    $('#streetName').val(streetName);
                    $('#streetNumber').val(streetNumber);
                    $('#zip').val(zip);
                    $('#city').val(city);
                    
                    $('#personal_tab').removeClass('active');
                    $('#personal_tab').removeAttr('href data-toggle');
                    $('#personalInfo').removeClass('active');
                    $('#personalInfo').addClass('fade');

                    $('#address_tab').removeClass('active');
                    $('#address_tab').removeAttr('href data-toggle');
                    $('#addressInfo').removeClass('active');
                    $('#addressInfo').addClass('fade');
                    
                    $('#payment_tab').removeClass('disabled');
                    $('#payment_tab').addClass('active');
                    $('#payment_tab').attr('href', '#paymentInfo');
                    $('#payment_tab').attr('data-toggle', 'tab');
                    $('#paymentInfo').removeClass('fade');
                    $('#paymentInfo').addClass('active');  
                    break; 
            
            case '3':$('#firstName').val(firstName);
                    $('#lastName').val(lastName);
                    $('#telephone').val(telephone); 
                    $('#streetName').val(streetName);
                    $('#streetNumber').val(streetNumber);
                    $('#zip').val(zip);
                    $('#city').val(city);
                    $('#accountOwner').val(accountOwner);
                    $('#iban').val(iban);
                    
                    $('#personal_tab').removeClass('active');
                    $('#personal_tab').removeAttr('href data-toggle');
                    $('#personalInfo').removeClass('active');
                    $('#personalInfo').addClass('fade');
                    
                    $('#address_tab').removeClass('active');
                    $('#address_tab').removeAttr('href data-toggle');
                    $('#addressInfo').removeClass('active');
                    $('#addressInfo').addClass('fade');
                    
                    $('#payment_tab').removeClass('active');
                    $('#payment_tab').removeAttr('href data-toggle');
                    $('#paymentInfo').removeClass('active');
                    $('#paymentInfo').addClass('fade'); 
                    
                    $('#result_tab').removeClass('disabled');
                    $('#result_tab').addClass('active');
                    $('#result_tab').attr('href', '#addressInfo');
                    $('#result_tab').attr('data-toggle', 'tab');
                    $('#result').removeClass('fade');
                    $('#result').addClass('active');  
                    break;
        }
        
    }

    $('#btnPersonalInfoCancel').click(function(){
        //Cookies
        $('#btnRegister').html('Continue Registration');
        var tab = 1;
        var firstName = $('#firstName').val();
        var lastName = $('#lastName').val();
        var telephone = $('#telephone').val();

        Cookies.set('tab',''+tab+'');
        Cookies.set('firstName',''+firstName+'');
        Cookies.set('lastName',''+lastName+'');
        Cookies.set('telephone',''+telephone+'');
    });

    $('#btnAddressInfoCancel').click(function(){
        //Cookies
        $('#btnRegister').html('Continue Registration');
        var tab = 2;
        var streetName = $('#streetName').val();
        var streetNumber = $('#streetNumber').val();
        var zip = $('#zip').val();
        var city = $('#city').val();

        Cookies.set('tab',''+tab+'');
        Cookies.set('streetName',''+streetName+'');
        Cookies.set('streetNumber',''+streetNumber+'');
        Cookies.set('zip',''+zip+'');
        Cookies.set('city',''+city+'');
    });

    $('#btnPaymentInfoCancel').click(function(){
        //Cookies
        $('#btnRegister').html('Continue Registration');
        var tab = 3;
        var accountOwner = $('#accountOwner').val();
        var iban = $('#iban').val();

        Cookies.set('tab',''+tab+'');
        Cookies.set('accountOwner',''+accountOwner+'');
        Cookies.set('iban',''+iban+'');
    });

 
    $('#btnPersonalInfo').click(function(){
        var error_name = '';
        var error_lname = '';
        var error_telephone = '';

        if($.trim($('#firstName').val()).length == 0){
            error_name = 'First name is required';
            $('#error_name').text(error_name);
            $('#firstName').addClass('has-error');
        }

        if($.trim($('#lastName').val()).length == 0){
            error_lname = 'Last name is required';
            $('#error_lname').text(error_lname);
            $('#lastName').addClass('has-error');
        }

        if($.trim($('#telephone').val()).length == 0){
            error_telephone = 'Telephone number is required';
            $('#error_telephone').text(error_telephone);
            $('#telephone').addClass('has-error');
        }

        if(error_name != '' || error_lname != ''){
            return false;
        }else {
            //Cookies
            var tab = 1;
            var firstName = $('#firstName').val();
            var lastName = $('#lastName').val();
            var telephone = $('#telephone').val();

            Cookies.set('tab',''+tab+'');
            Cookies.set('firstName',''+firstName+'');
            Cookies.set('lastName',''+lastName+'');
            Cookies.set('telephone',''+telephone+'');

            $('#personal_tab').removeClass('active');
            $('#personal_tab').removeAttr('href data-toggle');
            $('#personalInfo').removeClass('active');
            $('#personalInfo').addClass('fade');
            
            $('#address_tab').removeClass('disabled');
            $('#address_tab').addClass('active');
            $('#address_tab').attr('href', '#addressInfo');
            $('#address_tab').attr('data-toggle', 'tab');
            $('#addressInfo').removeClass('fade');
            $('#addressInfo').addClass('active');
        }
    });

    $('#previousAddressInfo').click(function(){
        $('#address_tab').removeClass('active');
        $('#address_tab').removeAttr('href data-toggle');
        $('#addressInfo').removeClass('active');
        $('#addressInfo').addClass('fade');

        $('#personal_tab').removeClass('disabled');
        $('#personal_tab').addClass('active');
        $('#personal_tab').attr('href', '#personalInfo');
        $('#personal_tab').attr('data-toggle', 'tab');
        $('#personalInfo').removeClass('fade');
        $('#personalInfo').addClass('active');
    });

    $('#btnAddressInfo').click(function(){

        var error_stName = '';
        var error_stNumber = '';
        var error_zip = '';
        var error_city = '';

        if($.trim($('#streetName').val()).length == 0){
            error_stName = 'Street name is required';
            $('#error_stName').text(error_stName);
            $('#streetName').addClass('has-error');
        }

        if($.trim($('#streetNumber').val()).length == 0){
            error_stNumber = 'Street number is required';
            $('#error_stNumber').text(error_stNumber);
            $('#streetNumber').addClass('has-error');
        }

        if($.trim($('#zip').val()).length == 0){
            error_zip = 'Zip number is required';
            $('#error_zip').text(error_zip);
            $('#zip').addClass('has-error');
        }

        if($.trim($('#city').val()).length == 0){
            error_city = 'City name is required';
            $('#error_city').text(error_city);
            $('#city').addClass('has-error');
        }

        if(error_stName != '' || error_stNumber != '' || error_zip != '' || error_city != ''){
            return false;
        }else {
            //Cookies
            var tab = 2;
            var streetName = $('#streetName').val();
            var streetNumber = $('#streetNumber').val();
            var zip = $('#zip').val();
            var city = $('#city').val();

            Cookies.set('tab',''+tab+'');
            Cookies.set('streetName',''+streetName+'');
            Cookies.set('streetNumber',''+streetNumber+'');
            Cookies.set('zip',''+zip+'');
            Cookies.set('city',''+city+'');

            $('#address_tab').removeClass('active');
            $('#address_tab').removeAttr('href data-toggle');
            $('#addressInfo').removeClass('active');
            $('#addressInfo').addClass('fade');
            
            $('#payment_tab').removeClass('disabled');
            $('#payment_tab').addClass('active');
            $('#payment_tab').attr('href', '#paymentInfo');
            $('#payment_tab').attr('data-toggle', 'tab');
            $('#paymentInfo').removeClass('fade');
            $('#paymentInfo').addClass('active');
        }
    });

    $('#previousPaymentInfo').click(function(){
        $('#payment_tab').removeClass('active');
        $('#payment_tab').removeAttr('href data-toggle');
        $('#paymentInfo').removeClass('active');
        $('#paymentInfo').addClass('fade');

        $('#address_tab').removeClass('disabled');
        $('#address_tab').addClass('active');
        $('#address_tab').attr('href', '#addressInfo');
        $('#address_tab').attr('data-toggle', 'tab');
        $('#addressInfo').removeClass('fade');
        $('#addressInfo').addClass('active');
    });

    $('#btnPaymentInfo').click(function(){

        var error_accOwner = '';
        var error_IBAN = '';

        if($.trim($('#accountOwner').val()).length == 0){
            error_accOwner = 'Account Owner name is required';
            $('#error_accOwner').text(error_accOwner);
            $('#accountOwner').addClass('has-error');
        }

        if($.trim($('#iban').val()).length == 0){
            error_IBAN = 'IBAN is required';
            $('#error_IBAN').text(error_IBAN);
            $('#iban').addClass('has-error');
        }

        if(error_accOwner != '' || error_IBAN != ''){
            return false;
        }else {
            $('#btnPaymentInfo').html('Please wait...!');
            //Cookies
            var tab = 3;
            var accountOwner = $('#accountOwner').val();
            var iban = $('#iban').val();

            Cookies.set('tab',''+tab+'');
            Cookies.set('accountOwner',''+accountOwner+'');
            Cookies.set('iban',''+iban+'');

            var client = {};
            client = {
                name : $('#firstName').val(),
                lastName : $('#lastName').val(),
                telephone: $('#telephone').val(),
                streetName: $('#streetName').val(),
                streetNumber: $('#streetNumber').val(),
                zip: $('#zip').val(),
                city: $('#city').val(),
                accountOwner: $('#accountOwner').val(),
                iban: $('#iban').val(),
            }

            $.ajax({
                type:"POST",
                url:"/register",
                headers:{"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')},
                data : JSON.stringify(client),
                contentType: "application/json",
                success: function(response){
                    $(response).each(function(key, value){
                        if(value.status ==200){

                            $('#payment_tab').removeClass('active');
                            $('#payment_tab').removeAttr('href data-toggle');
                            $('#paymentInfo').removeClass('active');
                            $('#paymentInfo').addClass('fade');
                            
                            $('#result_tab').removeClass('disabled');
                            $('#result_tab').addClass('active');
                            $('#result_tab').attr('href', '#addressInfo');
                            $('#result_tab').attr('data-toggle', 'tab');
                            $('#result').removeClass('fade');
                            $('#result').addClass('active');

                            $('#resultAlert').addClass("alert alert-success");
                            $('#resultAlert').attr('role', 'alert');
                            $('#resultAlertHeading').addClass("alert-heading");
                            $('#resultAlertHeading').text('Great! You have been registered.');
                            $('#resultAlertMsg').text(value.msg);
                        }else{
                            $('#payment_tab').removeClass('active');
                            $('#payment_tab').removeAttr('href data-toggle');
                            $('#paymentInfo').removeClass('active');
                            $('#paymentInfo').addClass('fade');
                            
                            $('#result_tab').removeClass('disabled');
                            $('#result_tab').addClass('active');
                            $('#result_tab').attr('href', '#addressInfo');
                            $('#result_tab').attr('data-toggle', 'tab');
                            $('#result').removeClass('fade');
                            $('#result').addClass('active');

                            $('#resultAlert').addClass("alert alert-danger");
                            $('#resultAlertHeading').addClass("alert-heading");
                            $('#resultAlertHeading').text('Ups! You have an error in your registration.');
                            $('#resultAlertMsg').text(value.msg);
                        }
                    });
                }
            });
        
        }
    });

    $('#previousResultInfo').click(function(){
        $('#result_tab').removeClass('active');
        $('#result_tab').removeAttr('href data-toggle');
        $('#result').removeClass('active');
        $('#result').addClass('fade');

        $('#payment_tab').removeClass('disabled');
        $('#payment_tab').addClass('active');
        $('#payment_tab').attr('href', '#addressInfo');
        $('#payment_tab').attr('data-toggle', 'tab');
        $('#paymentInfo').removeClass('fade');
        $('#paymentInfo').addClass('active');
    });

    $('#btnResult').click(function(){
        //Removing Cookies
        Cookies.remove('firstName');
        Cookies.remove('lastName');
        Cookies.remove('telephone');
        Cookies.remove('streetName');
        Cookies.remove('streetNumber');
        Cookies.remove('zip');
        Cookies.remove('city');
        Cookies.remove('accountOwner');
        Cookies.remove('iban');
        Cookies.remove('tab');

        $('#firstName').val("");
        $('#lastName').val("");
        $('#telephone').val(""); 
        $('#streetName').val("");
        $('#streetNumber').val("");
        $('#zip').val("");
        $('#city').val("");
        $('#accountOwner').val("");
        $('#iban').val("");

        $('#result_tab').removeClass('active');
        $('#result_tab').removeAttr('href data-toggle');
        $('#result').removeClass('active');
        $('#result').addClass('fade');

        $('#personal_tab').removeClass('disabled');
        $('#personal_tab').addClass('active');
        $('#personal_tab').attr('href', '#personalInfo');
        $('#personal_tab').attr('data-toggle', 'tab');
        $('#personalInfo').removeClass('fade');
        $('#personalInfo').addClass('active');
    });

});