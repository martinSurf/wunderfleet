import React, { Component } from 'react';
import FormHeader from './FormHeader';
import FormBody from './FormBody';

import './formCSS.css';

export default class Form extends Component{
    render(){
        return(
            <div>
                <form method="post">
                    <div className="row">
                        <FormHeader />
                        <FormBody />
                    </div>
                </form>
            </div>
        );
    }
}

