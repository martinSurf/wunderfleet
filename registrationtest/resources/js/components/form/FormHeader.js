import React, { Component } from 'react';

export default class FormHeader extends Component{
    render(){
        return(
            <div className="col-lg-4 indicator">
                <ul className="nav nav-tabs">
                    <li className="nav-item">
                        <a className="nav-link active" id="personal_tab"><i className="far fa-check-circle"></i> Personal Information</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link disabled" id="address_tab"><i className="far fa-check-circle"></i> Address Information</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link disabled" id="payment_tab"><i className="far fa-check-circle"></i> Payment Information</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link disabled" id="result_tab"><i className="far fa-check-circle"></i> Result</a>
                    </li>
                </ul>
            </div>
        );
    }
}

